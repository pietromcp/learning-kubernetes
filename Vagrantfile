def define_vm(config, name, baseBox, baseBoxVersion, ip, diskSize = nil, primary = false)
  config.vm.define name, primary: primary, autostart: primary do |node|
    node.vm.box = baseBox
    node.vm.box_version = baseBoxVersion
    unless diskSize.nil?
      node.disksize.size = diskSize
    end
    node.vm.network :private_network, ip: ip, :netmask => "255.255.0.0"
    node.vm.hostname = name
    node.vm.provider "virtualbox" do |vb|
      vb.name = name
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
      vb.customize ["modifyvm", :id, "--groups", "/learning-kubernetes"]
      vb.memory = 2048
      vb.cpus = 2
    end
  end
end

def define_k8s_vm(config, namePrefix, i, ip)
  define_vm config, "#{namePrefix}#{i}", "pietrom/debian9-kubernetes", "1.0.3", ip
end

Vagrant.configure("2") do |config|
  system "vagrant plugin install vagrant-disksize" unless Vagrant.has_plugin? "vagrant-disksize"  

  define_vm config, "k8s-gym", "pietrom/debian9-docker", "1.0.1", "172.29.0.100"

  define_vm config, "empty-debian", "pietrom/debian9", "1.0.1", "172.29.0.101"

  define_vm config, "control-tower", "pietrom/debian9-kubernetes", "1.0.3", "172.29.0.19"

  (0..3).each do |i|
    define_k8s_vm config, "kube", i, "172.29.1.#{10 + i}"
  end

  (0..2).each do |i|
    define_k8s_vm config, "sphere", i, "172.29.2.#{10 + i}"
  end

  (0..2).each do |i|
    define_k8s_vm config, "prism", i, "172.29.3.#{10 + i}"
  end
end