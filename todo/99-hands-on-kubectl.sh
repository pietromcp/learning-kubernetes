# Publish clock-api
kubectl create namespace clock
kubectl create deployment clock-api --image registry.gitlab.com/pietrom/clock-api:52 --namespace clock
kubectl apply --namespace clock -f descriptors/clock-api-node-port.yml